<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified']);

Route::middleware(['auth', 'verified'])
    ->controller(\App\Http\Controllers\ActivityController::class)
    ->group(callback: function () {
        Route::get('dashboard', 'index')->name('dashboard');
        Route::get('problem/one', 'activityOne')->name('problemOne');
        Route::get('problem/two', 'activityTwo')->name('problemTwo');
        Route::get('problem/three', 'activityThree')->name('problemThree');
    });

require __DIR__.'/auth.php';
