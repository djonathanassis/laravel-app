<?php

namespace App\Http\Controllers;

use App\Actions\ActivityAction;
use App\Models\Book;
use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Support\Facades\Request;
use Inertia\Inertia;
use Inertia\Response;

class ActivityController extends Controller
{

    public function index(): Response
    {
        return Inertia::render('Dashboard');
    }

    public function activityOne(): Response
    {
        return Inertia::render('Problem/One', (new ActivityAction())->executeOne());
    }

    public function activityTwo(): Response
    {
        return Inertia::render('Problem/Two', (new ActivityAction())->executeTwo());
    }

    public function activityThree(): Response
    {
        return Inertia::render('Problem/Three', (new ActivityAction())->executeThree());
    }
}
