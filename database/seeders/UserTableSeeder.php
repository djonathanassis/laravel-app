<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        User::query()->create([
            'name'           => 'Maria',
            'email'          => 'professor@gmail.com',
            'password'       => bcrypt('password'),
            'type'           => 'P',
            'remember_token' => null,
        ]);

        User::query()->create([
            'name'           => 'João',
            'email'          => 'aluno@gmail.com',
            'password'       => bcrypt('password'),
            'type'           => 'A',
            'remember_token' => null,
        ]);
    }
}
