<?php

namespace App\Actions;

use App\Models\Book;
use App\Models\User;
use Exception;

class ActivityAction
{

    public function executeOne(float $chico = 1.50, float $juca = 1.10)
    {
        $years = 0;
        while ($juca <= $chico)
        {
            $chico += 0.02;
            $juca += 0.03;
            $years++;
        }
        return [ 'years' => $years ];
    }

    public function executeTwo()
    {
        $user = User::all();
        $book = Book::all();

       return ['users' => $user, 'books' => $book];
    }


    public function executeThree($min = 1, $max = 9): array
    {
        $matrix = [];
        for ($i = 0; $i < 5; $i++)
        {
            for ($j = 0; $j < 5; $j++)
            {
                $matrix[$i][$j] = random_int($min, $max);
            }
        }

        $matrixPar = [];
        $matrixImpar = [];

        foreach ($matrix as $value) {
            foreach ($value as $v) {
                if(($v % 2) == 0 ){
                    $matrixPar[] = $v;
                }else{
                    $matrixImpar[] = $v;
                }
            }
        }

        return ['item' => ['matriz' => $matrix,  'matrizPar' => $matrixPar,  'matrizImpar' => $matrixImpar]];
    }
}
